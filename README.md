# GitLab CI/CD Components

[Includeable](https://docs.gitlab.com/ee/ci/yaml/includes.html) [GitLab CI/CD](https://docs.gitlab.com/ee/topics/build_your_application.html) 
configuration for tasks in different situations.

## Reusable CI/CD Component Libraries

| Library                          | Uses                           |
|----------------------------------|--------------------------------|
| [**infra.ci.lib.common**][1]     | General CI/CD tasks and jobs   |
| [**infra.ci.lib.containers**][2] | Container builds, pushes, etc. |
| [**infra.ci.lib.node**][3]       | Node.js CI/CD                  |
| [**infra.ci.lib.python**][4]     | Python CI/CD                   |

GitLab's [CI/CD pipeline components](https://docs.gitlab.com/ee/ci/components/) allow configuration to be generalized
for all projects. Include components in pipeline files like this:

```yaml
include:
  - component: gitlab.com/alexlab-cloud/infra/ci/lib/common/bootstrap.gitlab-ci@main
```

This syntax will pull the CI configuration from the
[`alexlab-cloud/infra/ci/components/common/base-stages.gitlab-ci.yml`](https://gitlab.com/alexlab-cloud/infra/ci/lib/common/-/blob/main/templates/base-stages.gitlab-ci.yml?ref_type=heads) file at `main`. Mutliple components can be included at a time to compose pipelines from reusable parts:

```yaml
include:
  - component: gitlab.com/alexlab-cloud/infra/ci/lib/common/bootstrap.gitlab-ci@main
  - component: gitlab.com/alexlab-cloud/infra/ci/lib/common/changesets-release.gitlab-ci@main
```

## Development

### Style

All custom symbols/names that aren't part of the [GitLab CI/CD YAML syntax](https://docs.gitlab.com/ee/ci/yaml/#environment)
should be wrapped in quotes. This includes things like stage names (e.g. '.base-stage:build') and job names (e.g. 'job:sast-semgrep').
This is to help distinguish actual CI/CD keywords from names coined during pipeline design.

## Resources

### Documentation

- [GitLab CI/CD components docs](https://docs.gitlab.com/ee/ci/components/)
- [Semantic Release](https://semantic-release.gitbook.io/semantic-release/recipes/ci-configurations/gitlab-ci)

### Helpful Projects

- [VAZ Projects / GitLab Components](https://gitlab.com/vaz-projects/gitlab/-/tree/main/templates?ref_type=heads)

---

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

---

[1]: https://gitlab.com/alexlab-cloud/infra/ci/lib/common
[2]: https://gitlab.com/alexlab-cloud/infra/ci/lib/containers
[3]: https://gitlab.com/alexlab-cloud/infra/ci/lib/node
[4]: https://gitlab.com/alexlab-cloud/infra/ci/lib/python
